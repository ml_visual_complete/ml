import csv

urls = open('majestic_million.urls.txt','w')
flag = 0 #skip first header row
with open('majestic_million.csv') as csvfile:
    readCSV = csv.reader(csvfile, delimiter=',')
    for row in readCSV:
        if flag == 0:
            flag = 1
            continue
        urls.write('https://www.'+row[2]+'\n')
urls.close()
       
