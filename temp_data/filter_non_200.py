import subprocess
import requests
import thread
import threading
import sys

threadLock = threading.Lock()
fileLock = threading.Lock()
threads = []
thread_count = 30

print 'Usage: <urlfile.txt> will output <urlfile.txt>_200_ok_urls.txt'

file_name = sys.argv[1]
good_urls = open(file_name+'_200_ok_urls.txt','w')
all_urls = open(file_name).read().split('\n')[:-1]
print 'Total urls: ',len(all_urls)

def get_status_code(url):
   print 'checking: ', url
   try:
       r = requests.head(url)
       print 'Time taken: ',r.elapsed.total_seconds(), ' url: ', url
       return r.status_code
   except requests.ConnectionError:
       print("failed to connect")
   return 400

def check_url(url):
    if get_status_code(url) == 200:
        fileLock.acquire()
        good_urls.write(url+'\n')
        good_urls.flush()
        fileLock.release()

class myThread (threading.Thread):
   def __init__(self, threadID, name):
      threading.Thread.__init__(self)
      self.threadID = threadID
      self.name = name
   def run(self):
      while True:
          # Get lock to synchronize threads
          threadLock.acquire()
          if len(all_urls) == 0: 
              threadLock.release()
              print 'No work for me: ', self.threadID
              return
          cur_url = all_urls.pop()
          # Free lock to release next thread
          threadLock.release()
          check_url(cur_url)
          #check_url('http://www.'+cur_url)
          #check_url('https://www.'+cur_url)
          print 'remaining url: ',len(all_urls)

def create_thread(n=thread_count):
    for i in range(n):
        threads.append(myThread(i,'thread-%d'%i))

def start_threads(n=thread_count):
    for t in threads:
        print "Starting " + t.name
        t.start()

def join_threads(n=thread_count):
    for t in threads:
        print 'joining: ', t.name
        t.join()

create_thread()
start_threads()
join_threads()

good_urls.close()

print 'Check out 200_ok_urls.txt file'
