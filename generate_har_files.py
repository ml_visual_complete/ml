
import os
import subprocess
import time


file_name = 'ScriptedMeasurements.feature'
script_file = open(file_name).read()


urls = open('200_ok_urls.txt').read().split()[:-1]
counter = 1
total_urls = len(urls)
for url in urls:
	print 'Processing (%d/%d): '%(counter,total_urls), url
	counter += 1
	new_folder = '.\\all_data\script_injection'+url.replace('/','_').replace(':','_').replace('.','_')
	if os.path.exists(new_folder):
		print 'Already preocessed, skipping'
		continue
	os.system('rm -rf .\Firefox\measurements\*')
	new_sf = script_file.replace('REPLACE_URL_HERE',url)
	retry_count = 1
	while retry_count:
		open('.\\resources\\features\\'+file_name,'w').write(new_sf)
		print 'Starting measurement'
		#import pdb;pdb.set_trace()
		cmd = '.\\bin\\run.sh -b "Firefox" -t "@remote_script_available"'
		print 'Executing cmd: ', cmd
		print os.system(cmd)
		#print 'sleeping for 100 seconds'
		#time.sleep(100)
		retry_count -= 1
		print 'Finding count of timers, grepping'
		os.system('rm -f 1.txt')
		print os.system('grep PENDING_TIMERS .\Firefox\measurements\script_injection\\results\har.json |wc -l > 1.txt')
		metrics_count = int(open('1.txt').read().strip())
		if metrics_count == 0:
			print 'could not find any custom metrics for ', url, ' retry count: ', retry_count
			continue
		else:
			print 'metrics count : ', metrics_count
			cp_cmd = 'cp -r .\Firefox\measurements\script_injection '+new_folder
			print 'running: ', cp_cmd
			os.system(cp_cmd)
			print 'Success ', url, ' check all_data folder '
			break
		