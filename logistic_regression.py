import cPickle, gzip
import numpy as np
import math

# Load the dataset
f = gzip.open('mnist.pkl.gz', 'rb')
train_set, valid_set, test_set = cPickle.load(f)
#train_set[0][0]
#print valid_set
f.close()

numClasses = 10
#target one hot encoded vector for every class
target = []

#10x10 matrix of 0s
for i in range(numClasses):
    target.append([0,0,0,0,0,0,0,0,0,0])

#fills one on diagonals
for i in range(numClasses):
    target[i][i] = 1

weightsPerClass = []

allData = []

#Error -already fixed :)
#all training data set
for i in range(len(train_set[0])):
    #error fixed
    #do not get rid of the extra parentheses, concat takes a tuple
    allData.append(np.concatenate( (np.array([0]),train_set[0][i]) ))
    
#all label data set for the training data
labelData = train_set[1]
#to be list of length 10
net = []
#total gradient descent over all examples
gradDesc = []
#to be list of length 10
y = []
#weigths for all the different features
weights = []
#alpha is the learning rate
alpha = 1.0

#first index is 0 or 1 for example or label
#second index is 0 to 50,000 examples or 50,000 labels

for i in range( len(allData) ):
    #add 0 to front of every data in allData
    np.insert(allData[i], 0, 0.0)

#appends 785 weights
for i in range( len(allData[0]) ):
    weightsPerClass.append(0.0)

#append weights 10 times    
for i in range(numClasses):
    weights.append(list(weightsPerClass))


def calc_net(currData):
    global numClasses, weights, net, allData
    #run through ten times, compute 10 weights
    #this net is for 10 differrent classes
    for i in range(numClasses):
        net.append(np.dot(weights[i], allData[currData]))

def calc_y():
    global numClasses, y, net
    
    netSum = 0.0   
    
    for i in range(numClasses):
        netSum += math.exp(net[i])
    print "netsum: " + str(netSum)
    for i in range(numClasses):
        y.append( float(math.exp(net[i])) / float(netSum) )
    
def calc_gradientDescent(i):
    global allData, labelData, y, net, gradDesc, weights
   
    gradDescTemp = []
    targetClassIndx = labelData[i]
     
    calc_net(i)
    calc_y()
    diff = list(np.array(target[targetClassIndx]) - np.array(y))    
    print "y: " + str(y) 
    print "targetClassIndx: " + str(target[targetClassIndx])
    
    y = []
    net = []
    
    for j in range(len(allData[i])):   
        updatedDiff = np.dot(diff, allData[i][j])        
        gradDescTemp.append(updatedDiff)          
                
    gradDesc = np.array(gradDescTemp)
    gradDesc = gradDesc.transpose()
       
def StochasticGradientDescent():
    global gradDesc, alpha, weights
    #taking the negative so that we can add it to weights
        
    #training for 100 time epochs, but need to 
    #determine a stopping condition      

    for t in range(100):        
        #all examples in the data set 
        ##change later to all examples##
        for i in range(10):
            calc_gradientDescent(i)          
            gradDesc = np.dot(gradDesc, alpha)                        
            weights = np.add(weights, gradDesc)
        
StochasticGradientDescent()

#and then run through the StochasticGradientDescent for different timestamps
