import numpy as np
from sklearn import linear_model
from sklearn.externals import joblib

from sklearn.utils import Bunch
import csv
import time
import pickle

training_data = 'ml_train_data.txt'
def load_data_2():
    with open(training_data) as csv_file:
        data_file = csv.reader(csv_file)
        temp = next(data_file)
        n_samples = int(temp[0])
        n_features = int(temp[1])
        target_names = np.array(temp[2:])
        data = np.empty((n_samples, n_features))
        target = np.empty((n_samples,), dtype=np.int)

        start_time = time.time()
        for i, ir in enumerate(data_file):
            try:
                data[i] = np.asarray(ir[:-1], dtype=np.int)
                target[i] = np.asarray(ir[-1], dtype=np.int)
                if i > 0 and i % 1000 == 0:
                    processing_time = time.time()
                    print 'Progress: (%d/%d)' % (i, n_samples)
                    print 'time taken (in seconds) for 1000 records: ', processing_time - start_time
                    start_time = processing_time
                    break
            except Exception,e:
                print e
                print 'line (+1?): ', i

    return data, target, target_names

def load_data_1():
    data, target, target_names = load_data_2()
    fdescr = open(training_data).read()
    print 'Creating Bunch'
    return Bunch(data=data, target=target,
                 target_names=target_names,
                 DESCR=fdescr,
                 feature_names=['result'])

# import some data to play with
vc = load_data_1()
print 'loading is done...'
X = vc.data
from sklearn.preprocessing import RobustScaler
scaler = RobustScaler()
X = scaler.fit_transform(X)
Y = vc.target
print 'Scaling done'

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25,
                                                    shuffle=True, random_state=42)
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score

clf = MLPClassifier(hidden_layer_sizes=(100,100,100), max_iter=200, alpha=0.1,
                     solver='sgd', verbose=10,  random_state=21,tol=0.000000001)
clf.fit(X_train, y_train)
y_pred = clf.predict(X_test)
print 'acc:',accuracy_score(y_test, y_pred)

print 'Pickling the model'
joblib.dump(clf, 'vc_ml_model_clf_neural.pkl')  # load as  knn = joblib.load('filename.pkl')
print 'python pickle'
pf = open('vc_ml_model_cls_python_neural.pkl','w')
pickle.dump(clf, pf)  # read as
pf.close()
print 'python pickle ... done'

## we open the file for reading
#fileObject = open(file_Name,'r')
## load the object from the file into var b
#b = pickle.load(fileObject)

#import pdb;pdb.set_trace()
x_test = clf.predict(X_test)
compare = (x_test == y_test )


