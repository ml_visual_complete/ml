
import numpy as np
import csv
import time
import pickle
from optparse import OptionParser
import glob
import bisect
import json
import os

compare_file = None
compare_record = {}
compare_fields = ['URL', 'ML_VC_TIME', 'SYNTH_VC_TIME', 'ML_SNAPSHOT_FILE', 'SYNTH_SNAPSHOT_FILE']


def load_vc_data(test_file):
    print 'loading test data'
    with open(test_file) as csv_file:
        data_file = csv.reader(csv_file)
        temp = next(data_file)
        n_samples = int(temp[0])
        n_features = int(temp[1])
        target_names = np.array(temp[2:])
        data = np.empty((n_samples, n_features))
        target = np.empty((n_samples,), dtype=np.int)

        st_time = time.time()
        for i, ir in enumerate(data_file):
            try:
                data[i] = np.asarray(ir[:-1], dtype=np.int)
                target[i] = np.asarray(ir[-1], dtype=np.int)
                if i and i % 1000 == 0:
                    processing_time = time.time()
                    print 'Progress: (%d/%d)' % (i, n_samples)
                    print 'time taken (in seconds) for 1000 records: ', processing_time - st_time
                    st_time = processing_time
            except Exception, e:
                print e
                print 'line (+1?): ', i
    print 'loading test data ... done'
    return data, target, target_names


def find_ml_screenshot(vc_time, folder_path):
    bmps = glob.glob(folder_path+'/results/processing/*.bmp')
    timestamps = [int(bmp.split('/')[-1].split('.bmp')[0]) for bmp in bmps]
    timestamps.sort()
    try:
        tid = bisect.bisect_right(timestamps, vc_time)
        if tid == len(timestamps):
            tid = len(timestamps)-1

        result = folder_path+'/results/processing/'+str(timestamps[tid])+'.bmp'
        # compare_record['ML_VC_TIME'] = timestamps[tid] - timestamps[0]
        # print 'ML visual complete time: ', compare_record['ML_VC_TIME']
        # using from screenshot time
    except Exception, e:
        print e
        result = folder_path+'/results/processing/'+str(timestamps[-1])+'.bmp'
    return result


def get_synth_agent_stats(folder_path, test_file):
    har_file = json.load(open(folder_path+'/results/har.json'))['pages'][0]
    print '-'*80
    screen_shot_json = har_file['_pageScreenshots'][0]
    print 'Synth agent based stats'
    print 'Synth visual complete time: ', screen_shot_json['taken_ms']
    compare_record['SYNTH_VC_TIME'] = screen_shot_json['taken_ms']
    synth_snap_shot_file = folder_path.split('/')[0]+screen_shot_json['fileName'].split('measurements')[1]\
        .replace('\\', '/')
    cmd = 'cp %s %s' % (synth_snap_shot_file, test_file.split('.')[0]+'_synth.'+synth_snap_shot_file.split('.')[1])
    print '*** running cmd: ', cmd
    os.system(cmd)

    print 'Screenshot file: ', synth_snap_shot_file
    compare_record['SYNTH_SNAPSHOT_FILE'] = synth_snap_shot_file
    print 'URL: ', har_file['_URL']
    compare_record['URL'] = har_file['_URL']
    print '-'*80
    return screen_shot_json['taken_ms']


def find_the_image(test_file, line_number, folder_path):
    with_time_file = test_file.split('.')[0]+'_with_time.txt'
    lines = open(with_time_file).read().split('\n')[:-1]
    vc_time = int(lines[line_number].split(',')[0])
    print '-'*80
    print 'ML based stats'
    vc_snap_shot = find_ml_screenshot(vc_time, folder_path)
    beg_time = int(lines[0].split(',')[0])
    compare_record['ML_VC_TIME'] = vc_time - beg_time  # using from screenshot time
    print 'ML visual complete time: ', compare_record['ML_VC_TIME']
    cmd = 'cp %s %s' % (vc_snap_shot, test_file.split('.')[0]+'_ml.'+vc_snap_shot.split('.')[1])
    print '*** running cmd: ', cmd
    os.system(cmd)
    print 'Screenshot file: ', vc_snap_shot
    compare_record['ML_SNAPSHOT_FILE'] = vc_snap_shot
    get_synth_agent_stats(folder_path, test_file)


def find_vc_time(pkl_file, test_file, folder_path, loaded=False, folder_prefix=None):
    global compare_record
    global compare_file
    if folder_prefix is None:
        compare_file = open('compare_ml_synth.csv', 'a')
        compare_file_not_found = open('compare_ml_synth_not_found.csv', 'a')
    else:
        compare_file = open('compare_ml_synth_%s.csv' % folder_prefix, 'a')
        compare_file_not_found = open('compare_ml_synth_not_found_%s.csv' % folder_prefix, 'a')
    data, target, target_names = load_vc_data(test_file)
    if not loaded:
        print 'loading model'
        clf = pickle.load(open(pkl_file))
        print 'loading model ... done'
    else:
        clf = pkl_file
    prediction = 0

    for i, test_record in enumerate(data):  # try to parallelize
        prediction = clf.predict([test_record])
        if prediction == 1:
            print 'Got VC time at record number: ', i
            compare_record = {}
            find_the_image(test_file, i, folder_path)
            compare_file.write(','.join(map(str, [compare_record[key] for key in compare_fields]))+'\n')
            break
    if prediction == 0:
        print 'Could not find VC time **********************'
        compare_file_not_found.write(test_file+'\n')
    compare_file.close()
    compare_file_not_found.close()


if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-d', '--test-data-file', default=None, dest="test_data_file",
                      help='Test data file')
    parser.add_option('-p', '--pickle-file', default=None, dest="pkl_file",
                      help='pickle file path')
    parser.add_option('-f', '--folder-path', default=None, dest="folder_path",
                      help='folder path which contains results under it ex: A_SIVA ')

    (options, args) = parser.parse_args()

    if not options.test_data_file and not options.pkl_file and not options.folder_path:
        parser.print_help()
        print 'All arguments are needed'
        parser.print_help()
        exit(0)

    start_time = time.time()
    find_vc_time(options.pkl_file, options.test_data_file, options.folder_path)
    print 'Time taken: ', time.time() - start_time
