from scipy.stats import pearsonr
import matplotlib.pyplot as plt
import os


def generate_png_file(compare_csv_file):
    records = open(compare_csv_file).read().split('\n')[:-1]

    x = []
    y = []
    for record in records:
        fields = record.split(',')
        x.append(int(fields[1]))
        y.append(int(fields[2]))

    print 'pearson correlation coefficient: ', pearsonr(x, y)

    z = range(len(x))
    plt.plot(z, x, 'b')
    plt.text(z[0], x[0], 'ML VC Time')
    plt.text(z[0], y[0], 'Synthetic VC Time')
    plt.plot(z, y, 'r')
    category = compare_csv_file.split('/')[-1].split('.')[0]
    plt.title(category)
    plt.grid(True)
    plt.ylabel('VC Time')
    plt.xlabel('Record #')
    graph_file = compare_csv_file.split('.csv')[0]+'.png'
    plt.savefig(graph_file)
    print 'check: ', graph_file
    os.system('open '+graph_file)

#  compare_csv_file = sys.argv[1]

'''
$ for i in `ls validate_categories_tgzs/temp_*/*.csv |grep -v not_found`;
do echo $i; python pearson_correlation.py $i ;done

validate_categories_tgzs/temp_arts/compare_ml_synth_arts.csv
pearson correlation coefficient:  (0.82658218931078875, 3.8670176887127894e-06)
validate_categories_tgzs/temp_business/compare_ml_synth_business.csv
pearson correlation coefficient:  (0.74348529130740404, 0.00062449042943658406)
validate_categories_tgzs/temp_computers/compare_ml_synth_computers.csv
pearson correlation coefficient:  (0.69972417092826911, 0.0025493606293791856)
validate_categories_tgzs/temp_games/compare_ml_synth_games.csv
pearson correlation coefficient:  (0.5476042829442137, 0.0083393379610487255)
validate_categories_tgzs/temp_health/compare_ml_synth_health.csv
pearson correlation coefficient:  (0.58422930242806237, 0.00087566539014802389)
validate_categories_tgzs/temp_home/compare_ml_synth_home.csv
pearson correlation coefficient:  (0.31160353280916714, 0.1212372327679177)
validate_categories_tgzs/temp_kidsteens/compare_ml_synth_kidsteens.csv
pearson correlation coefficient:  (0.40892578602223817, 0.013274821781954901)
validate_categories_tgzs/temp_news/compare_ml_synth_news.csv
pearson correlation coefficient:  (0.34674865874482474, 0.1341902978318549)
validate_categories_tgzs/temp_recreation/compare_ml_synth_recreation.csv
pearson correlation coefficient:  (0.65284997214335594, 0.00040379188538632553)
validate_categories_tgzs/temp_reference/compare_ml_synth_reference.csv
pearson correlation coefficient:  (0.22862713032622387, 0.2081733032538387)
validate_categories_tgzs/temp_regional/compare_ml_synth_regional.csv
pearson correlation coefficient:  (0.6316720819251106, 0.00093049450483663585)
validate_categories_tgzs/temp_science/compare_ml_synth_science.csv
pearson correlation coefficient:  (0.56192931635905718, 0.00036194311285530479)
validate_categories_tgzs/temp_shopping/compare_ml_synth_shopping.csv
pearson correlation coefficient:  (0.90594964472238182, 6.5977267845825556e-09)
validate_categories_tgzs/temp_society/compare_ml_synth_society.csv
pearson correlation coefficient:  (0.78759242553242792, 8.852608743867874e-08)
validate_categories_tgzs/temp_sports/compare_ml_synth_sports.csv
pearson correlation coefficient:  (0.27510891892479905, 0.19323049908221848)
validate_categories_tgzs/temp_world/compare_ml_synth_world.csv
pearson correlation coefficient:  (0.550217628327761, 0.0035881016334000019)
'''
