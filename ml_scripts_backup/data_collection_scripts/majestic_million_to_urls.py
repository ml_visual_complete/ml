import csv
from optparse import OptionParser

'''
script to convert majestic million csv file (http://downloads.majestic.com/majestic_million.csv)
to urls file used by generate_feature_file.py
'''


def main(opts):
    urls = open(opts.url_file, 'w')
    flag = 0  # skip first header row
    with open(opts.majestic_million_csv) as csv_file:
        read_csv = csv.reader(csv_file, delimiter=',')
        for row in read_csv:
            if flag == 0:
                flag = 1
                continue
            urls.write('https://www.'+row[2]+'\n')
    urls.close()

if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-c', '--majestic-million-csv-file', default=None, dest="majestic_million_csv", 
                      help='path to majestic_million.csv file downloaded from '
                           'http://downloads.majesticseo.com/majestic_million.csv')
    parser.add_option('-u', '--url-file-to-generate', default=None, dest="url_file",
                      help='file to generate urls')

    (options, args) = parser.parse_args()

    if None in [options.majestic_million_csv, options.url_file]:
        print 'Need -c -u options'
        parser.print_help()
        exit(0)

    main(options)
