from optparse import OptionParser
import os


def main(opts):
    start = opts.start_index
    end = opts.end_index
    prefix = opts.prefix
    golden_urls = opts.golden_urls
    cucumber_test_folder = opts.cucumber_test_folder
    
    header = open('ScriptedMeasurements.feature.header').read()
    footer = open('ScriptedMeasurements.feature.footer').read()
    
    grand_cmd_file = 'grand_cmd.sh'
    gf = open(grand_cmd_file, 'w')
    step_size = 500
    for i in range(start, end, step_size):
        start = i
        end = i+step_size
        script_feature_file = 'ScriptedMeasurements_%d_%d.feature' % (start, end)
        f = open(script_feature_file, 'w')
        f.write(header)
        get_cmd_file = 'get_data_cmd_%d_%d.sh' % (start, end)
        cf = open(get_cmd_file, 'w')

        command_file = 'bin/run.sh -b "Firefox" -t '
        urls = open(golden_urls).read().split('\n')[:-1]

        for j, url in enumerate(urls[start:end]):
            url_scenario = footer.replace('A_ID_TAG', prefix+'_'+str(i+start)).replace('REPLACE_URL_HERE', url)
            command_file += '@%s_%d,' % (prefix, j+start)
            f.write(url_scenario)
        f.close()
        cf.write(command_file[:-1]+'\n')
        cf.close()
        cur_dir = os.getcwd()
        gf.write('cp %s/%s %s/%s\n' % (cur_dir, get_cmd_file, cucumber_test_folder, get_cmd_file))
        gf.write('cp %s/%s %s/resources/features/%s \n' % (cur_dir, script_feature_file, cucumber_test_folder,
                                                           script_feature_file))
        gf.write('echo "running: start = %d end = %d"\n' % (start, end))
        gf.write('cd %s/; sh %s\n' % (cucumber_test_folder, get_cmd_file))
        print 'check : ', get_cmd_file, ' and ', script_feature_file
    gf.close()
    print 'check: ', grand_cmd_file
    print 'can run: sh %s' % grand_cmd_file

if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-s', '--start-index', default=0, dest="start_index", type="int",
                      help='start index in urls file where to begin measurements')
    parser.add_option('-e', '--end-index', default=0, dest="end_index", type="int",
                      help='end index of the url in the url file (exclusive)')
    parser.add_option('-p', '--prefix', default=None, dest="prefix",
                      help='prefix of the url category ex: arts,games,computers etc')
    parser.add_option('-u', '--url-file', default=None, dest="golden_urls",
                      help='file containing one url per line')
    parser.add_option('-c', '--cucumber-tests-folder', default=None, dest="cucumber_test_folder",
                      help='cucumber test folder path ')

    (options, args) = parser.parse_args()

    if None in [options.golden_urls, options.prefix, options.start_index,
                options.end_index, options.cucumber_test_folder]:
        parser.print_help()
        print 'all options are needed'
        exit(0)

    main(options)
