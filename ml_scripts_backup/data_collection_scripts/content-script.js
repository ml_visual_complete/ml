/*
 * Copyright (c) AppDynamics, Inc., and its affiliates
 * 2016
 * All Rights Reserved
 * THIS IS UNPUBLISHED PROPRIETARY CODE OF APPDYNAMICS, INC.
 * The copyright notice above does not evidence any actual or intended publication of such source code
 */

((function() {
    // true when page statistics for the current page has been sent
    var statsSent = false;

    var initialURL = /http:\/\/(localhost|127\.0\.0\.1):8888\/static\/blank.html/;
    var isInitialPage = function() {
        // firefox has a bug where the exclude field in manifest is not followed
        return initialURL.test(document.documentURI);
    };


    // Register ourselves with the background script
    chrome.runtime.sendMessage({message: 'CONTENT_SCRIPT_REGISTRATION'}, function(response) {
        overwriteUserAgent(response.data.userAgent);
    });

    window.addEventListener('ML_FEATURE_DATA_EVENT', function(ml_event) {
        if (isInitialPage()) {
            console.log("On initial page, ignoring requests");
        } else {
            chrome.runtime.sendMessage(ml_event.detail);
        }

    });
   

    window.addEventListener('beforeunload', function() {
        if (isInitialPage()) {
            console.log("On initial page, ignoring requests");
        } else {
            sendPageStatistics(false);
            chrome.runtime.sendMessage({
                message: 'BEFORE_UNLOAD',
                data: {
                    timestampMs: Date.now()
                }
            });
        }

    });

    window.addEventListener('unload', function() {
        if (isInitialPage()) {
            console.log("On initial page, ignoring requests");
        } else {
            sendPageStatistics(false);
            chrome.runtime.sendMessage({
                message: 'UNLOAD',
                data: {
                    timestampMs: Date.now()
                }
            });
        }
    });

    // listener for messages from the background script.
    var backgroundListener = function(request, sender, sendResponse) {
        console.log("Received " + JSON.stringify(request));
        if (isInitialPage()) {
            console.log("On initial page, ignoring requests");
        } else if (request.message == 'COLLECT_PAGE_DATA') {
            console.log("Sending page statistics");
            sendPageStatistics(true);
        }
    };

    var sendPageStatistics = function (requested) {
        if (statsSent) {
            return;
        }

        statsSent = true;
        var data = {
            title: document.title,
            domCount: document.getElementsByTagName('*').length,
            navTiming: window.performance.timing,
            timestampMs: Date.now()
        };

        // window.chrome.loadTime is not available on all platforms
        if (window.chrome && window.chrome.loadTimes) {
            data.firstPaint = Math.round(window.chrome.loadTimes().firstPaintTime * 1000);
        } else {
            data.firstPaint = -1;
        }

        chrome.runtime.sendMessage({ message: 'PAGE_DATA', data: data, requested: requested });
    };

    chrome.runtime.onMessage.addListener(backgroundListener);

    // This is necessary to add the PTST tag in UA returned by navigator.userAgent. Selenium will set the
    // custom user agent, but will not append the PTST tag.
    var overwriteUserAgent = function (userAgent) {
        if (!userAgent) {
            return;
        }

        var actualCode =  '(' + function() {
            Object.defineProperties(Navigator.prototype, {
                userAgent: {
                    value: 'userAgentString',
                    configurable: false,
                    enumerable: true,
                    writable: false
                }
            });
        } + ')();';
        actualCode = actualCode.replace('userAgentString', userAgent);

        var s = document.createElement('script');
        s.innerHTML = actualCode;
        document.documentElement.appendChild(s);
    };
})());
