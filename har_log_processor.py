#!/usr/bin/env python

import glob
import json
import os
from optparse import OptionParser

max_seconds = 300
ml_record_counter = 0
field_list = ['CURRENT_TIME',
              'PENDING_TIMERS',
              'XHR_CALLS',
              'MAX_DURATION',
              'RESOURCE_DIFF_COUNT',
              'PAGE_LOAD_TIME',
              'FIRST_PAINT_TIME',
              'RENDER_TIME',
              'DOM_INTERACTIVE_TIME',
              'DOM_LOADING_TIME',
              'RESOURCE_ACCELERATION',
              'VIEW_PORT_IMAGE_COUNT']


def dummy_ml_record(fields_length=9, call_every_msec=100):
    return [0] * (fields_length * (1000 / call_every_msec) * max_seconds)


def process_har_log(har, ml_train_data):
    global ml_record_counter
    try:
        har_json = json.load(open(har), 'utf-8')
    except ValueError, e:
        print '**** cant load: ', har, ' Exception: ', e
        return
    if 'pages' not in har_json:
        print 'No pages found, skipping: ', har
        return

    if len(har_json['pages']) > 1:  # have seen some issues where first page is just blank html etc
        print 'More than one page found, skipping: ', har
        return

    for page in har_json['pages']:
        if '_pageScreenshots' in page and len(page['_pageScreenshots']) == 0:
            print 'No _visualComplete found or it is zero, skipping: ', har, page['_pageScreenshots']
            continue

        visual_complete_time = page['_visualComplete']
        if visual_complete_time in [0, -1]:
            print 'invalid visual complete time'
            continue

        fields = {}

        for field in field_list:
            fields[field] = []

        feature_length = len(field_list)
        try:
            if page['_customMetrics'] is None:
                continue
            for custom_metric in page['_customMetrics']:
                fields[custom_metric['name']].append(custom_metric['value'])
        except Exception, e:
            print '*' * 100, '\n', e, '\n', '*' * 100

        min_length = 9999999
        for key in fields:
            min_length = min(len(fields[key]), min_length)

        ml_record = dummy_ml_record(len(field_list))
        ml_record_index = 0
        last_record_index = min_length-1
        for i in range(min_length):
            feature_record = []
            current_time = 0
            for field in field_list:
                if field == 'CURRENT_TIME':
                    current_time = fields[field][i]
                    continue
                feature_record.append(fields[field][i])

            ml_record[ml_record_index:ml_record_index + len(feature_record)] = feature_record
            ml_record_index += feature_length
            ml_record_counter += 1
            if current_time < visual_complete_time and i != last_record_index:
                # for last record we are setting 1 as label
                ml_train_data.write(','.join(map(str, ml_record)) + ',0\n')
            else:
                ml_train_data.write(','.join(map(str, ml_record)) + ',1\n')


def get_max_seconds(folder):
    global max_seconds
    max_seconds = 100  # found after 14700 features has no importance so capping it
    '''
    print 'Computing max seconds'
    os.system(
        'rm -f /tmp/123; for i in `ls %s/*/results/har.json`;
        do grep CURRENT_TIME $i |wc -l; done  | sort -n |tail -1 >/tmp/123' % folder)
    max_seconds = int(subprocess.check_output(['cat', '/tmp/123']).split(' ')[-1].strip())
    max_seconds = int(math.ceil(max_seconds / 10.0))
    print 'Computed max seconds: ', max_seconds
    '''
    return


def process_measurements(folder):
    get_max_seconds(folder)
    ml_train_data_file = 'ml_training_data.txt'
    ml_train_data = open(ml_train_data_file, 'w')
    ml_train_data.write('1,1,"result"\n')  # will be replaced with sed -i '' '1s/1,1,/count,fcount,/g'
    for har in glob.glob(folder + '/*/results/har.json')[:1400]:
        print 'Processing har file: ', har
        process_har_log(har, ml_train_data)
    ml_train_data.close()
    feature_length = max_seconds * 10 * len(field_list)
    print 'Checkout ml_training_data.txt file record count and length: ', ml_record_counter, feature_length
    print 'Replacing first line '
    os.system("sed -i '' '1s/1,1,/%d,%d,/g' %s" % (ml_record_counter, feature_length, ml_train_data_file))
    print 'Replacing first line ... done\nCheck: %s' % ml_train_data_file


if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-f', '--measurement-folder-path', default=None, dest="folder_path",
                      help='Measurement folder absolute path containing folders with results/har.json file')

    (options, args) = parser.parse_args()

    if options.folder_path is None:
        parser.print_help()
        print 'Need -f option'
        exit(0)
    process_measurements(options.folder_path)

# python har_log_processor.py -f all_data
