import csv
from validate_model import compare_fields
import os
from optparse import OptionParser


def generate_html_file(csv_file):
    html_file_name = csv_file.split('.')[0]+'.html'
    html_file = open(html_file_name, 'w')

    reader = csv.reader(open(csv_file))

    column = {}
    for h in compare_fields:
        column[h] = []

    for row in reader:
        for h, v in zip(compare_fields, row):
            column[h].append(v)

    # generate html
    html_file.write('<html><title>Comparision of ML vs Synthetic agent VC</title><body>')
    html_file.write('<table style="width:100%"><tr><th>Machine Learning</th><th>Synthetic Agent</th></tr>')

    export_tgz_file = csv_file.split('.')[0]+'.tgz'
    zip_cmd = 'tar zcvf %s %s %s ' % (export_tgz_file, csv_file, html_file_name)
    for i in range(len(column[column.keys()[0]])):
        folder_path = column[compare_fields[3]][i].split('/results')[0]
        html_file.write('<tr><td>Record: %d %s</td>' % (i+1, folder_path))
        html_file.write('<td>Record: %d %s</td></tr>' % (i+1, folder_path))
        row_str = '<tr><td>URL: %s</td><td>URL: %s</td></tr>' % (column[compare_fields[0]][i],
                                                                 column[compare_fields[0]][i])  # url
        row_str += '<tr><td>ML VC Time: %s</td><td>Synth VC Time: %s</td></tr>' % (column[compare_fields[1]][i],
                                                                                   column[compare_fields[2]][i])
        zip_cmd += ' ' + column[compare_fields[3]][i] + ' ' + column[compare_fields[4]][i]
        row_str += '<tr><td><img src="./%s" width="75%%"/></td>' % (column[compare_fields[3]][i])  # ml snap shot
        row_str += '<td><img src="./%s" width="75%%"/></td></tr>' % (column[compare_fields[4]][i])  # synth snap shot
        html_file.write(row_str)
    html_file.write('</table></body></html>')
    html_file.close()

    print 'Zipping: ', zip_cmd
    os.system(zip_cmd)

    print 'Check: ', html_file_name, export_tgz_file

if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-c', '--compare-csv-file', default=None, dest="csv_file",
                      help='compare csv file path ex: compare_ml_synth.csv')

    (options, args) = parser.parse_args()
    if options.csv_file is None:
        print 'specify -c '
        parser.print_help()
        exit(0)
    generate_html_file(options.csv_file)
