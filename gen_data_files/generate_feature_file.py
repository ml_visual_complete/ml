import sys
print '------------------------------------------------------------------'
print 'Takes start and end as command line args and generates ScriptedMeasurements.feature.range files and get_data_cmd.range.sh files <- this one you need to run manually by copying .feature files into agent-cucumber-tests/resources/features/\n'
print '------------------------------------------------------------------'
start = int(sys.argv[1])
end = int(sys.argv[2])

header = open('ScriptedMeasurements.feature.header').read()
footer = open('ScriptedMeasurements.feature.footer').read()

gf = open('grand_cmd.sh', 'w')
step_size = 500
for i in range(start, end, step_size):
    start = i
    end = i+step_size
    script_feature_file = 'ScriptedMeasurements.feature_%d_%d'%(start,end)
    f = open(script_feature_file,'w')
    f.write(header)
    get_cmd_file = 'get_data_cmd_%d_%d.sh'%(start, end)
    cf = open(get_cmd_file, 'w')
    command_file_header = 'bin/run.sh -b "Firefox" -t '
    command_file = 'bin/run.sh -b "Firefox" -t '
    for i, url in enumerate(open('golden_urls.txt').read().split('\n')[:-1][start:end]):
        url_scenario = footer.replace('ID_TAG',str(i+start)).replace('REPLACE_URL_HERE', url)
        command_file += '@A_%d,' % (i+start)
        f.write(url_scenario)
    f.close()
    cf.write(command_file[:-1]+'\n')
    cf.write('for i in `ls /c/ML/agent-cucumber-tests/Firefox/measurements/`; do echo $i; rm -rf $i/results/processing; done\n')
    cf.write('cp -r /c/ML/agent-cucumber-tests/Firefox/measurements/* /c/golden_data/\n')
    cf.close()
    gf.write('cp /c/gen_data/files/%s /c/ML/agent-cucumber-tests/get_data_cmd.sh \n' % get_cmd_file)
    gf.write('cp /c/gen_data/files/%s /c/ML/agent-cucumber-tests/resources/features/ScriptedMeasurements.feature \n' % script_feature_file)
    gf.write('echo "running: start = %d end = %d"\n'%(start,end));
    gf.write('cd /c/ML/agent-cucumber-tests/; sh get_data_cmd.sh\n')
    print 'checkout get_data_cmd.sh, ScriptedMeasurements.feature ',start, end
gf.close()


