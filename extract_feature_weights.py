import pickle
from optparse import OptionParser


def main(pkl_file):
    field_list = ['PENDING_TIMERS',
                  'XHR_CALLS',
                  'MAX_DURATION',
                  'RESOURCE_DIFF_COUNT',
                  'PAGE_LOAD_TIME',
                  'FIRST_PAINT_TIME',
                  'RENDER_TIME',
                  'DOM_INTERACTIVE_TIME',
                  'DOM_LOADING_TIME',
                  'RESOURCE_ACCELERATION',
                  'VIEW_PORT_IMAGE_COUNT']
    
    clf = pickle.load(open(pkl_file))
    i = 0
    f = {}
    for fld in field_list:
        f[fld] = []
    
    for j in clf.feature_importances_:
        f[field_list[i]].append(float(j))
        i += 1
        if i == len(field_list):
            i = 0
    
    to_sort = {}
    for k in f.keys():
        to_sort[k] = (sum(f[k])/float(len(f[k])))
    
    import operator
    sorted_x = sorted(to_sort.items(), key=operator.itemgetter(1))
    sorted_x.reverse()
    
    print 'Features which contributed most: '
    print '*'*80
    print '# feature_name                   weight'
    print '-'*80
    for j, k in enumerate(sorted_x):
        print j+1, '%-30s' % k[0], k[1]
        print '-'*80
    print '*'*80

if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-p', '--pickle-file', default=None, dest="pkl_file",
                      help='pickle file path')

    (options, args) = parser.parse_args()

    if not options.pkl_file:
        parser.print_help()
        print '-p is needed'
        parser.print_help()
        exit(0)
    main(options.pkl_file)

'''
python extract_feature_weights.py -p "~/appdy/ML/bitbucket/ml/backup_models/
                               vc_ml_randomforest_2017_09_28_15_12_0.987500540634_M_0_999.pkl"
Features which contributed most:
********************************************************************************
# feature_name                   weight
--------------------------------------------------------------------------------
1 PENDING_TIMERS                 0.000116285479142
--------------------------------------------------------------------------------
2 DOM_LOADING_TIME               0.000114655810637
--------------------------------------------------------------------------------
3 MAX_DURATION                   9.90733860689e-05
--------------------------------------------------------------------------------
4 PAGE_LOAD_TIME                 8.94573007232e-05
--------------------------------------------------------------------------------
5 VIEW_PORT_IMAGE_COUNT          8.44919100776e-05
--------------------------------------------------------------------------------
6 DOM_INTERACTIVE_TIME           8.33223708878e-05
--------------------------------------------------------------------------------
7 RESOURCE_ACCELERATION          7.62019885639e-05
--------------------------------------------------------------------------------
8 RENDER_TIME                    6.60442097605e-05
--------------------------------------------------------------------------------
9 XHR_CALLS                      6.59820082596e-05
--------------------------------------------------------------------------------
10 RESOURCE_DIFF_COUNT            6.36469721032e-05
--------------------------------------------------------------------------------
11 FIRST_PAINT_TIME               5.75062923832e-05
--------------------------------------------------------------------------------
********************************************************************************
'''
