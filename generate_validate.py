import time
from optparse import OptionParser
import glob
import har_log_processor_validate
import validate_model
import os
import pickle
import generate_compare_gist
from pearson_correlation import generate_png_file


def process(pkl_file, folder_path, ml_record_length, folder_prefix):
    test_cases = glob.glob(folder_path+'/'+folder_prefix+'*')
    print 'loading model'
    clf = pickle.load(open(pkl_file))
    print 'loading model ... done'
    print test_cases
    for test_case in test_cases:
        pages_meta_data = har_log_processor_validate.process_measurements(test_case, ml_record_length)
        for page_meta_data in pages_meta_data:
            validate_file, validate_file_with_time, ml_record_counter, ml_record_len = page_meta_data
            validate_model.find_vc_time(clf, validate_file, test_case, True, folder_prefix)

if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-p', '--pickle-file', default=None, dest="pkl_file",
                      help='pickle file path')
    parser.add_option('-f', '--folder-path', default=None, dest="folder_path",
                      help='folder path which contains results under it ex: results')
    parser.add_option('-l', '--ml-record-length', default=None, dest="ml_record_length",
                      help='the length of ml record: ex: 38760', type=int)
    parser.add_option('-x', '--folder-prefix', default='*', dest="folder_prefix",
                      help='generate diff only for prefixed folder ex: A, health')

    (options, args) = parser.parse_args()

    if not options.pkl_file and not options.folder_path and not options.ml_record_length:
        parser.print_help()
        print 'All arguments are needed'
        parser.print_help()
        exit(0)

    if options.folder_prefix == '*':
        cmd = 'rm -f compare_ml_synth.csv compare_ml_synth_not_found.csv'
    else:
        cmd = 'rm -f compare_ml_synth_%s.csv compare_ml_synth_not_found_%s.csv' % (options.folder_prefix,
                                                                                   options.folder_prefix)
    print 'executing cmd: ', cmd
    os.system(cmd)
    start_time = time.time()
    process(options.pkl_file, options.folder_path, options.ml_record_length, options.folder_prefix)
    print 'Generating html & tgz file'
    generate_compare_gist.generate_html_file('compare_ml_synth_%s.csv' % options.folder_prefix)
    save_tgz_folder = 'validate_categories_tgzs'
    os.system('mv compare_ml_synth_*%s* %s' % (options.folder_prefix, save_tgz_folder))
    os.system('cd %s; mkdir temp_%s; cd temp_%s; tar zxvf ../compare_ml_synth_%s.tgz' % (save_tgz_folder,
                                                                                         options.folder_prefix,
                                                                                         options.folder_prefix,
                                                                                         options.folder_prefix))
    os.system('open %s/temp_%s/compare_ml_synth_%s.html' % (save_tgz_folder, options.folder_prefix,
                                                            options.folder_prefix))
    generate_png_file('%s/temp_%s/compare_ml_synth_%s.csv' % (save_tgz_folder, options.folder_prefix,
                                                              options.folder_prefix))
    print 'Time taken: ', time.time() - start_time


# python generate_validate.py -f validate -l 38760 -p use_vc_ml_randomforest_2017_09_16_09_24_0.979053172715.pkl -x arts
