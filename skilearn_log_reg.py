import numpy as np
from sklearn import linear_model
from sklearn.utils import Bunch
import csv
import time
import pickle
from sklearn.model_selection import train_test_split
from optparse import OptionParser
from sklearn.model_selection import cross_val_score
from sklearn import metrics
import os
from sklearn.preprocessing import normalize

training_data = 'NEED TO PASS DATA FILE via -d <data-file>'
clf = None
acc_score = 0
f1_score = 0
ml_data = None
X = None
Y = None

def load_vc_data(test=False):
    with open(training_data) as csv_file:
        data_file = csv.reader(csv_file)
        temp = next(data_file)
        n_samples = int(temp[0])
        n_features = int(temp[1])
        target_names = np.array(temp[2:])
        data = np.empty((n_samples, n_features))
        target = np.empty((n_samples,), dtype=np.int)

        start_time = time.time()
        for i, ir in enumerate(data_file):
            try:
                data[i] = np.asarray(ir[:-1], dtype=np.int)
                target[i] = np.asarray(ir[-1], dtype=np.int)
                if i and i % 1000 == 0:
                    processing_time = time.time()
                    print 'Progress: (%d/%d)' % (i, n_samples)
                    print 'time taken (in seconds) for 1000 records: ', processing_time - start_time
                    start_time = processing_time
                    if test:
                        print '************** Just for testing ********************'
                        break
            except Exception,e:
                print e
                print 'line (+1?): ', i

    return data, target, target_names


def create_bunch():
    data, target, target_names = load_vc_data()
    fdescr = open(training_data).read()
    print 'Creating Bunch'
    return Bunch(data=data, target=target,
                 target_names=target_names,
                 DESCR=fdescr,
                 feature_names=['result'])


def build_the_model_reg():
    global clf
    global X
    X = normalize(X)
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25,
                                                        shuffle=True, random_state=42)
    clf = linear_model.LogisticRegression(C=1e5, class_weight='balanced') # balanced giving better performance
    clf.fit(X_train, y_train)
    print 'model built'

    print 'Test accuracy'
    y_pred = clf.predict(X_test)
    compute_metrics(y_test, y_pred)

    print 'Cross validating the model'
    cv(clf)

    ''' igore for now
    print '\nTrain accuracy'
    y_pred = clf.predict(X_train)
    compute_metrics(y_train, y_pred)
    '''
def build_the_model_SGD():
    from sklearn import linear_model
    global clf

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25,
                                                        shuffle=True, random_state=42)
    clf = linear_model.SGDClassifier(class_weight='balanced',) # balanced giving better performance
    clf.fit(X_train, y_train)
    print 'model SGD Classifer built'

    print 'Test accuracy'
    y_pred = clf.predict(X_test)
    compute_metrics(y_test, y_pred)

def build_the_model_linearSVC():
    from sklearn.svm import LinearSVC
    global clf

    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25,
                                                        shuffle=True, random_state=42)
    clf = LinearSVC(C=1e5)
    clf.fit(X_train, y_train)
    print 'model built'

    print 'Test accuracy'
    y_pred = clf.predict(X_test)
    compute_metrics(y_test, y_pred)


def build_the_model_SVC():
    from sklearn.svm import SVC
    global clf

    global X
    test_components = 10
    print 'Performing dimensionality reduction components: ', test_components
    from sklearn.decomposition import TruncatedSVD
    svd = TruncatedSVD(n_components=test_components, n_iter=7, random_state=42)
    #X = svd.fit_transform(X,Y)
    print 'Dimensionality reduction done'
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25,
                                                        shuffle=True, random_state=42)

    from sklearn.multiclass import OneVsRestClassifier
    clf = OneVsRestClassifier(SVC(C=1e5, max_iter=100, cache_size=1000, kernel='linear',), n_jobs=-1)
    clf.fit(X_train, y_train)
    print 'model built'

    print 'Test accuracy'
    y_pred = clf.predict(X_test)
    compute_metrics(y_test, y_pred)

def build_the_model_gaussian():
    from sklearn.naive_bayes import GaussianNB
    global clf
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25,
                                                        shuffle=True, random_state=42)

    clf = GaussianNB()
    clf.fit(X_train, y_train)
    print 'model built'
    print 'Test accuracy'
    y_pred = clf.predict(X_test)
    compute_metrics(y_test, y_pred)

def build_the_model_RF():
    from sklearn.ensemble import RandomForestClassifier
    global clf
    X_train, X_test, y_train, y_test = train_test_split(X, Y, test_size=0.25,
                                                        shuffle=True, random_state=42)

    clf = RandomForestClassifier()
    clf.fit(X_train, y_train)
    print 'model RF built'
    print 'Test accuracy'
    y_pred = clf.predict(X_test)
    compute_metrics(y_test, y_pred)

models = {'svc': build_the_model_SVC,
          'gaussian': build_the_model_gaussian,
          'logisticregression': build_the_model_reg,
          'linearsvc': build_the_model_linearSVC,
          'sgd': build_the_model_SGD,
          'randomforest': build_the_model_RF}


def load_data():
    global X,Y, ml_data

    if os.path.exists('ml_data.data'):
        ml_data = pickle.load(open('ml_data.data'))
        X = ml_data.data
        Y = ml_data.target
    else:
        ml_data = create_bunch()
        X = ml_data.data
        Y = ml_data.target
        #pickle.dump(ml_data, open('ml_data.data', 'w'))
    print 'data loading done'


def build_the_model(model):
    if model in models:
        load_data()
        models[model]()
    else:
        print 'invalid model specified, allowed are: ', models.keys()


def compute_metrics(y_test, y_pred):
    global acc_score
    acc_score = metrics.accuracy_score(y_test, y_pred)
    global f1_score
    f1_score = metrics.f1_score(y_test, y_pred, average='binary')
    print 'Accuracy: %.5f'% acc_score
    print 'Precision: %.5f' % metrics.precision_score(y_test, y_pred, average='binary')
    print 'Recall: %.5f' % metrics.recall_score(y_test, y_pred, average='binary')
    print 'F1 score: %.5f' % f1_score
    print 'Confusion matrix: [ tn fp fn tp ]', metrics.confusion_matrix(y_test, y_pred).ravel()


def save_model(model):
    # save the model
    print 'Pickling the model'
    pickle_file = 'vc_ml_%s_%s_%s.pkl' % (model, time.strftime("%Y_%m_%d_%H_%M"), f1_score)
    pf = open(pickle_file, 'w')
    pickle.dump(clf, pf)  # read as
    pf.close()
    print 'python pickle ... done, saved as: ', pickle_file

def evaluate_cv(pickle_file):
    global ml_data
    ml_data = create_bunch()
    clf = pickle.load(open(pickle_file))
    cv(clf)

def cv(clf):
    print 'cross validating the model: '
    from sklearn.model_selection import ShuffleSplit
    scores = cross_val_score(clf, normalize(ml_data.data), ml_data.target, cv=ShuffleSplit(test_size=0.25),scoring='f1')
    print 'Scores: ',scores
    print("Accuracy: %0.2f (+/- %0.2f)" % (scores.mean(), scores.std() * 2))


if __name__ == '__main__':
    usage = "usage: %prog [options] arg"
    parser = OptionParser(usage)
    parser.add_option('-c','--cross-validate',default=None, dest="cross_val_model", help='Evaluate with 5 fold cross validation')
    parser.add_option('-b', '--build-model', default=False, action="store_true", dest="build_model", help='Will build and save the model')
    parser.add_option('-d', '--train-data-file', default='ml_train_data.txt', dest="train_data",
                      help='Training data file default(ml_train_data.txt)')
    parser.add_option('-m', '--model-to-build', default=None, dest="model",
                      help='which model to build one of ' + str(models.keys()))


    (options, args) = parser.parse_args()

    if not options.build_model and not options.cross_val_model :
        parser.print_help()
        print 'Atleast one argument is needed'
        exit(0)

    training_data = options.train_data
    start_time = time.time()
    if options.cross_val_model:
        evaluate_cv(options.cross_val_model)

    if options.build_model:
        build_the_model(options.model)
        save_model(options.model)
    print 'Time taken: ', time.time() - start_time




