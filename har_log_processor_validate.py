#!/usr/bin/env python

import sys
import glob
import json
import time
from datetime import datetime
from calendar import timegm


field_list = ['CURRENT_TIME',
              'PENDING_TIMERS',
              'XHR_CALLS',
              'MAX_DURATION',
              'RESOURCE_DIFF_COUNT',
              'PAGE_LOAD_TIME',
              'FIRST_PAINT_TIME',
              'RENDER_TIME',
              'DOM_INTERACTIVE_TIME',
              'DOM_LOADING_TIME',
              'RESOURCE_ACCELERATION',
              'VIEW_PORT_IMAGE_COUNT']
ml_record_len = 0


def dummy_ml_record():
    return [0] * ml_record_len


def get_starttime_epoch_seconds(started_date_time):
    a = datetime.strptime(started_date_time, "%Y-%m-%dT%H:%M:%S.%f+00:00")
    return timegm(time.strptime(started_date_time, "%Y-%m-%dT%H:%M:%S.%f+00:00")) * 1000 + (a.microsecond / 1000)


def process_har_log(har, test_case):
    try:
        har_json = json.load(open(har), 'utf-8')
    except ValueError, e:
        print '**** cant load: ', har, ' Exception: ', e
        return []
    if 'pages' not in har_json:
        print 'No pages found, skipping: ', har
        return []

    pages_meta_data = []
    page_counter = 0
    if len(har_json['pages']) > 1:
        print 'More than one pages found in the url, skipping for now'
        return []
    for page in har_json['pages']:
        if '_pageScreenshots' in page and len(page['_pageScreenshots']) == 0:
            print 'No _visualComplete found or it is zero, skipping: ', har, page['_pageScreenshots']
            continue

        start_epoch_mseconds = get_starttime_epoch_seconds(page['startedDateTime'])
        visual_complete_time = page['_visualComplete']
        if visual_complete_time in [0, -1]:
            print 'invalid visual complete time'
            continue

        validate_file = 'validate_train_data/%s_page_%d.txt' % (test_case, page_counter)
        ml_train_data = open(validate_file, 'w')
        validate_file_with_time = 'validate_train_data/%s_page_%d_with_time.txt' % (test_case, page_counter)
        ml_train_data_with_time = open(validate_file_with_time, 'w')
        page_counter += 1
        ml_record_counter = 0

        fields = {}

        for field in field_list:
            fields[field] = []

        feature_length = len(field_list)
        try:
            if page['_customMetrics'] is None:
                continue
            for custom_metric in page['_customMetrics']:
                fields[custom_metric['name']].append(custom_metric['value'])
        except Exception, e:
            print '*' * 100, '\n', e, '\n', '*' * 100

        min_length = 9999999
        for key in fields:
            min_length = min(len(fields[key]), min_length)

        ml_record = dummy_ml_record()
        ml_record_index = 0
        last_record_index = min_length-1
        for i in range(min_length):
            feature_record = []
            current_time = 0
            for field in field_list:
                if field == 'CURRENT_TIME':
                    current_time = fields[field][i]
                    continue
                feature_record.append(fields[field][i])

            ml_record[ml_record_index:ml_record_index + len(feature_record)] = feature_record
            ml_record_index += feature_length
            ml_record_counter += 1
            if current_time < visual_complete_time and i != last_record_index:
                # for last record we are setting 1 as label
                ml_train_data.write(','.join(map(str, ml_record)) + ',0\n')
                ml_train_data_with_time.write(str(current_time+start_epoch_mseconds)+','+','.join(map(str, ml_record))
                                              + ',0\n')
            else:
                ml_train_data.write(','.join(map(str, ml_record)) + ',1\n')
                ml_train_data_with_time.write(str(current_time+start_epoch_mseconds)+','+','.join(map(str, ml_record))
                                              + ',1\n')
                # NOT IGNORING return #ignoring rest of the records after visual complete
        ml_train_data.close()
        ml_train_data_with_time.close()
        import os
        os.system('echo "%s,%s,\'result\'" >/tmp/1 ; cat %s >> /tmp/1 ; mv /tmp/1 %s ' % (ml_record_counter, ml_record_len, validate_file, validate_file))
        pages_meta_data.append([validate_file, validate_file_with_time, ml_record_counter, ml_record_len])
        print 'Checkout %s %s file record count and length: ' % (validate_file, validate_file_with_time), ml_record_counter, ml_record_len
    return pages_meta_data


def process_measurements(folder, ml_record_length):
    global ml_record_len
    ml_record_len = ml_record_length
    test_case = folder.split('/')[1]
    print 'Folder: ', folder
    try:
        har = glob.glob(folder + '/results/har.json')[0]  # there will be only one har.json
        print 'Processing har file: ', har
        return process_har_log(har, test_case)
    except Exception, e:
        print e
        return []

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print 'Usage: < measurement folder absolute path > < ML record length 38760 (excluding "result")>'
        exit(1)

    process_measurements(sys.argv[1], int(sys.argv[2]))
