This document captures the details of how to get the "Visually Complete (VC) Time" of a page using "Machine Learning"
(ML) model. Wiki page can be found at https://singularity.jira.com/wiki/spaces/EUM/pages/297123164/Exploring+Machine+Learning+Techniques+to+determine+Visually+Complete+Time

**Broad steps**

1. Data collection
2. Model building
3. Finding VC time of a web page


**1. Data Collection**

We use synthetic-agent cucumber tests to capture "Custom Metrics" needed to build the ML model, by tweaking
"content-script.js" file to capture the custom events containing "feature" data. Data collection should be run
on a windows 2012 machine ( on mac one can use virtual box to create a windows 2012 instance )

Preparing Synthetic-agent:
    Edit the file synthetic-agent/agent/browser-extension/src/main/js/content-script.js by
    adding the code snippet containing event listener code listening for the event 'ML_FEATURE_DATA_EVENT' present
    in data_collection_scripts/content-script.js. Build the agent with the commands 'gradlew installDist' and 'gradlew -p installer distZip'.
    Transfer the "synthetic-agent-installer.zip" to the windows instance under C: and expand it.

Preparing "feature" files:
    The features needed for the ML model will be captured under the "custom metrics" field in har files. We need list of
    URLs for which we need har file one per line. Use "generate_feature_file.py" to generate the feature file
    containing necessary code and as well commands to be run to get the measurement data in a shell script. One can
    directly run the script to get the measurement data.

List of urls can be obtained from http://downloads.majesticseo.com/majestic_million.csv and use
"majestic_million_to_urls.py" to get the urls text file with one url per line and use it in
"generate_feature_file.py"

**2. Model building**
    Once all measurement data is collected, use "har_log_processor.py" to generate the training data. Once the
    training data is ready use "skilearn_build_models.py" to build the ML model. One need to pass the required
    params to build the model (ex: model to build [randomforest], training data file etc). The script will build
    the model and spit out the model statistics at the end (ex: accuracy, F1 score and the models serialized
     file, which is a python pickle file etc). This model file will be used to deduce the VC time of a new page
    when the har file is fed to the model.

**3. Finding VC time of a web page**
    Generate the har file for the page for which VC time needs to be deduced by following the same procedure as
    in step 1. Once the har file is generated use "generate_validate.py" to get the VC time.